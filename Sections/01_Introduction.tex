% !TEX root = ../Main.tex

\section{Introduction}
In \acl{CIS} a search agent connects users to information through multi-turn conversational interactions~\citep{gao2022neural,ren2021wizard,aliannejadi2019asking,yang2018response,tang2022re}.
Mixed initiative is a key aspect in \acl{CIS}: the search agent and user can both take the initiative at different times in a conversation~\citep{allen1999mixed,radlinski2017theoretical}. 
E.g., the search agent can improve its understanding of the user's information needs through a range of initiatives such as asking clarifying questions~\citep{Hamedwww2020,aliannejadi2019asking} or eliciting the user's preferences~\citep{sepliarskaia-2018-preference}.
%
Finding the right balance between initiative taking and leaving control to the user is an essential ingredient of \acl{CIS} and a complex problem~\citep{vakulenko2021large, aliannejadi2021analysing,wang2021controlling,wang2022simulating}.
In this paper, we propose a method for learning a \acfi{SIS} that helps a search agent decide whether to take the initiative by performing a certain action at each conversational turn.

We find that different levels of user initiative have an impact on \acl{SIS}, i.e., 
%that different levels of user initiative should give rise to different initiative levels on the search agent's part.
the search agent should adapt for different levels of user initiative.
Through an analysis of users' initiative levels in the \ac{WISE} dataset~\citep{ren2021wizard}, we find that users may exhibit different levels of initiative-taking. 
Specifically, we analyze the interactions between a search agent and users in the \ac{WISE} dataset, which contains annotations of actions taken by the user and search agent at each conversational turn.
We define a user's initiative level to be the ratio of the number of turns where the user takes the initiative over the total number of user turns in a conversation; a user's initiative level reflects the extent to which he or she proactively reveals aspects of their information need~\citep{azzopardi2018conceptualizing}.
We group users in the \ac{WISE} dataset into three bins, depending on their initiative level: low (ratios from 0.2 to 0.5), moderate (ratios from 0.5 to 0.8) and high (ratios from 0.8 to 1.0) initiative levels; see Figure~\ref{figure:ratios}.
For the conversations belonging to each of bins, we calculate the average ratio of the number of turns where the system takes the initiative over the total number of system turns in a conversation.
For bins corresponding to low, moderate and high levels of user initiative, the ratios are 0.27, 0.13, 0.07, respectively. 
It indicates that users with a low initiative level tend to be more inactive, which may require the search agent to take more initiative so as to get a proper understanding of the user’s information needs if facilitated;
conversely, users with a high initiative level tend to take more initiative to proactively reveal (parts of their) information need, so that the search agent needs to take less initiative to keep up with the user's pace and give answers.
%\moh{This paragraph provides a very good motivation for the work but the details are too much to be put in the Intro. I suggest to provide a higher level discussion here and then provide the details in another section in the paper.}

\begin{figure}[t]
\centering
\includegraphics[width=1\columnwidth]{Figures/figure1.png}
\caption{Histogram of the ratio of the number of turns where the user takes the initiative over the total number of user turns in the \ac{WISE} dataset~\citep{ren2021wizard}.}
\label{f1}
\label{figure:ratios}
\end{figure}

Previous work aiming at optimizing the \acl{SIS} is mainly based on supervised learning~\citep{ren2021wizard}.
However, previous work does not explicitly distinguish between users who display different levels of initiative-taking and thus does not consider the necessity for the \acl{SIS} to adapt for different levels of user initiative.
Also, existing training corpora are imbalanced in terms of the amount of datapoints they contain for different levels of user initiative, making it challenging for methods based on supervised learning to learn a \acl{SIS} to perform well at different levels of user initiative.

In this paper, we propose a \acfi{MAMI} framework for \ac{CIS}. 
\ac{MAMI} is used to learn an optimal \acl{SIS} that can take an appropriate action at each conversational turn given different user initiative levels, using self-play between a search agent and multiple (simulated) user agents.
Informed by the analysis of users' initiative levels described above, we choose to use three distinct simulated user agents, with low, moderate and high initiative levels, respectively.
%
In order to ensure the three simulated user agents provide the search agent with a reliable simulated environment and are able to display different levels of user initiative, \ac{MAMI} is based on the actor-critic framework. 
We regard the search agent and the three simulated user agents as actors (each simulated user agent has its own \acl{UIS}), and devise a centralized \emph{mixed-initiative critic} and three \emph{\acl{UIS} critics}:
\begin{enumerate}[leftmargin=*]
\item the \emph{mixed-initiative critic} jointly supervises the learning of the \acl{SIS} and \acl{UISs} by evaluating the cooperation between the search agent and each of the three user agents; and 
\item each of the three \acl{UIS} critics supervises a corresponding user agent to learn to adopt \acl{UIS} with a certain initiative level.
\end{enumerate}
Through \ac{MAMI}, the \acl{SIS} can be optimized by simulated interactions with \acl{UISs} of different initiative levels.

Our experimental results show that 
%a search agent optimized using \ac{MAMI} significantly outperforms the state-of-the-art method in terms of both automatic and human evaluation on the tasks of action prediction prediction, passage selection and response generation.
%Our results indicate that 
\ac{MAMI} aids the search agent to identify a more appropriate \acl{SIS} given different levels of user initiative, and hence to select or generate better responses.

The contributions of this paper can be summarized as follows:
\begin{itemize}[leftmargin=*]
 \item We propose a \acfi{MAMI} framework for \acl{CIS}, in which we learn an optimal \acl{SIS} that can take an appropriate action at each conversational turn when interacting with users with different initiative levels, using self-play between the search agent and the user agents.

 \item We base \ac{MAMI} on the actor-critic framework, and devise a centralized mixed-initiative critic and three \acl{UIS} critics to ensure that the three user agents provide the search agent with a reliable simulated environment and display different levels of user initiative.

 \item We conduct extensive experiments on the \ac{WISE} dataset, demonstrating that \ac{MAMI} aids the search agent to identify a more appropriate \acl{SIS} given different levels of user initiative, and hence leading to better passage selection and response generation.
 \end{itemize}